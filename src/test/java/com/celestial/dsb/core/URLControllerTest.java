/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

import com.celestial.dsb.core.ApplicationScopeHelper;
import com.celestial.dsb.core.URLController;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Selvyn
 */
public class URLControllerTest
{
    private URLController urlController;
    
    public URLControllerTest()
    {
    }
    
    @Before
    public void setUp()
    {
        urlController = new URLController();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public  void    bootstrap_db_connection()
    {
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        boolean result = ash.bootstrapDBConnection();

        assertThat(result, is(true) );
    }
    
    @Test
    public  void    save_link_to_db_using_controller()
    {
        String url = "wwww.google.co.uk";
        String desc = "There's just something about this google thing";
        String tags = "search engine, news, queen";
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        ash.bootstrapDBConnection();
        
        urlController.saveURL(url, desc, tags);
    }
}
